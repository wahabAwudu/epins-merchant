import Home from './components/home.vue'
import Lost from './components/lost.vue'
import Login from './components/auth/login.vue'
import Register from './components/auth/register.vue'
import ConfirmRegistration from './components/auth/confirmRegistration.vue'
import ValidateAccount from './components/auth/validateAccount.vue'
import PasswordResetEmail from './components/auth/passwordResetEmail.vue'
import PasswordResetEmailConfirm from './components/auth/passwordResetEmailConfirm.vue'
import PasswordResetConfirm from './components/auth/passwordResetConfirm.vue'
import ShowUser from './components/auth/showUser.vue'
import UpdateUser from './components/auth/updateUser.vue'
import DashboardHome from './components/dashboard/index.vue'
import BusinessDashboard from './components/dashboard/business.vue'


export default [
    { path: '/', component: Home, name: 'home' },

    // auth routes
    { path: '/access/login/', component: Login, name:'login', meta: { requiresGuest: true }},
    { path: '/access/register/', component: Register, name: 'register', meta: { requiresGuest: true }},
    { path: '/access/register/success/', component: ConfirmRegistration, name: 'confirm-registration', meta: { requiresGuest: true }},
    { path: '/access/verify-email/:key/', component: ValidateAccount, name: 'validate-account', meta: { requiresGuest: true }},
    { path: '/access/password-reset/', component: PasswordResetEmail, name: 'password-reset-email', meta: { requiresGuest: true }},
    { path: '/access/password-reset/email-sent/', component: PasswordResetEmailConfirm, name: 'password-reset-email-sent', meta: { requiresGuest: true }},
    { path: '/access/password-reset/confirm/:uid/:token/', component: PasswordResetConfirm, name: 'password-reset-confirm', meta: { requiresGuest: true }},
    { path: '/profile/', component: ShowUser, name: 'showUser', meta: { requiresAuth: true }},
    { path: '/profile/update/', component: UpdateUser, name: 'updateUser', meta: { requiresAuth: true }},

    // dashboard routes
    { path: '/dashboard/home/', component: DashboardHome, name: 'dashboard-home', meta: {requiresAuth: true}},
    { path: '/dashboard/businesses/:id/', component: BusinessDashboard, name: 'business-dashboard', meta: {requiresAuth: true}},
    // error routes
    { path: '*', component: Lost },
]