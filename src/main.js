import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import VeeValidate from 'vee-validate'
import sync from 'vue-router-sync'

// component imports
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"

// modules imports
import Routes from './router'
import store from './store'

Vue.use(BootstrapVue)
Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(VeeValidate)

const router = new VueRouter({
  routes: Routes,
})

// sync(store, router)

router.beforeEach((to, from, next) => {

  // check for either auth or guest routes
  if(to.meta.requiresAuth) {
    // check if user is logged in
    const authUser = JSON.parse(localStorage.getItem('authUser'))
    if(authUser) {
      next();
    } else {
      // go to login if user is not logged in
      next({name: 'login'})
    }
 
  } else {
    next();
  } // end condition

});

router.beforeEach((to, from, next) => {
  if (to.meta.requiresGuest) {
    const authUser = JSON.parse(localStorage.getItem('authUser'))
    if(!authUser) {
      next();
    }
    else {
      next({name: 'dashboard-home'});
    }
  } else {
    next();
  } // end condition
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
