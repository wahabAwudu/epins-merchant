import {
    LOGIN_BEGIN,
    LOGIN_ERROR,
    LOGIN_SUCCESS,
    SET_TOKEN,
    REMOVE_TOKEN,
} from './types'


const state = {
    authUser: null,
    loggedIn: false,
    loggedOut: false,
}


const mutations = {
    SET_TOKEN(state, payload) {
        state.authUser = payload
    },
    REMOVE_TOKEN(state) {
        state.authUser = null;
    }
}

const actions = {
    setAuthUser({commit}, payload) {
        commit('SET_TOKEN', payload)
    },
    removeAuthUser({commit}) {
        commit('REMOVE_TOKEN')
    }
}

export default {
    state: state,
    mutations: mutations,
    actions: actions
}