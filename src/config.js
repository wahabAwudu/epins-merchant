export const baseUrl = 'http://127.0.0.1:9000/api/v1/'

// auth endpoints
export const loginUrl = baseUrl + 'rest-auth/login/'
export const logoutUrl = baseUrl + 'rest-auth/logout/'
export const registerUrl = baseUrl + 'rest-auth/registration/'
export const authTokenUrl = baseUrl + 'rest-auth/registration/auth/v1/api-token-auth/'
export const verifyAccountUrl = baseUrl + 'rest-auth/registration/verify-email/'
export const passwordResetUrl = baseUrl + 'rest-auth/password/reset/'
export const passwordResetConfirmUrl = baseUrl + 'rest-auth/password/reset/confirm/'
export const refreshTokenUrl = baseUrl + 'api-token-refresh/'
export const userUrl = baseUrl + 'rest-auth/user/'

// project resources endpoints
export const customUsersUrl = baseUrl + 'users/'
export const countriesUrl = baseUrl + 'countries/'
export const businessUnitsUrl = baseUrl + 'business_units_users/'
export const vouchersUrl = baseUrl + 'vouchers/list_business_vouchers/'

// set request headers
export const getHeaders = function() {
    const authUser = JSON.parse(localStorage.getItem('authUser'));

    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'JWT ' + authUser.token
    };
    return headers
}